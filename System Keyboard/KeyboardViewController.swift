//
//  KeyboardViewController.swift
//  System Keyboard
//
//  Created by Liv  on 6/28/16.
//  Copyright © 2016 phillipsl. All rights reserved.
//

import UIKit

class KeyboardViewController: UIInputViewController {
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var dataLabel: UILabel!
    @IBOutlet weak var wordLabel: UILabel!
    
    @IBOutlet var nextKeyboardButton: UIButton!
    var customInterface: UIView!

    override func updateViewConstraints() {
        super.updateViewConstraints()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nib = UINib(nibName: "KeyboardUppercase", bundle: nil)
        let objects = nib.instantiateWithOwner(self, options: nil)
        view = objects[0] as! UIView;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }

    override func textWillChange(textInput: UITextInput?) {
        // The app is about to change the document's contents. Perform any preparation here.
    }

    override func textDidChange(textInput: UITextInput?) {
        // The app has just changed the document's contents, the document context has been updated.
    }
    
    @IBAction func nextKeyboardPressed(button: UIButton) {
        advanceToNextInputMode()
    }
    
    @IBAction func keyTapped(button: UIButton) {
        let string = button.titleLabel!.text
        (textDocumentProxy as UIKeyInput).insertText("\(string!)")
    }

    @IBAction func backTapped(button: UIButton) {
        (textDocumentProxy as UIKeyInput).deleteBackward()
    }
    
    @IBAction func spaceTapped(button: UIButton) {
        (textDocumentProxy as UIKeyInput).insertText(" ")
    }
    
    @IBAction func returnTapped(button: UIButton) {
        (textDocumentProxy as UIKeyInput).insertText("\n")
    }
    
    @IBAction func lowercaseKeyboard() {
        let nib = UINib(nibName: "KeyboardLowercase", bundle: nil)
        let objects = nib.instantiateWithOwner(self, options: nil)
        view = objects[0] as! UIView;
    }
    
    @IBAction func uppercaseKeyboard() {
        let nib = UINib(nibName: "KeyboardUppercase", bundle: nil)
        let objects = nib.instantiateWithOwner(self, options: nil)
        view = objects[0] as! UIView;
    }
    
    
    @IBAction func lowerAccentKeyboard() {
        
        //         Present the non-accented keys as grey before displaying the regular keyboard view:
        
        let nib = UINib(nibName: "KeyboardGreyLowerAccented", bundle: nil)
        let objects = nib.instantiateWithOwner(self, options: nil)
        view = objects[0] as! UIView;
        self.view.endEditing(true)
        
        //        Include 0.5 second pause on the grey screen:
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.upperAtoLowerA();
        }
    }
    
    @IBAction func upperAccentKeyboard(){
        
        //         Present the non-accented keys as grey before displaying the regular keyboard view:

        let nib = UINib(nibName: "KeyboardGreyUpperAccented", bundle: nil)
        let objects = nib.instantiateWithOwner(self, options: nil)
        view = objects[0] as! UIView;
        self.view.endEditing(true)
        
        //        Include 0.5 second pause on the grey screen:
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.5 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            self.lowerAtoUpperA();
        }
    }
    
    @IBAction func upperAtoLowerA(){
        let nib = UINib(nibName: "KeyboardLowerAccented", bundle: nil)
        let objects = nib.instantiateWithOwner(self, options: nil)
        self.view = objects[0] as! UIView;
    }
    
    @IBAction func lowerAtoUpperA(){
        let nib = UINib(nibName: "KeyboardUpperAccented", bundle: nil)
        let objects = nib.instantiateWithOwner(self, options: nil)
        self.view = objects[0] as! UIView;
    }
    
    
    @IBAction func numericKeyboard() {
        let nib = UINib(nibName: "KeyboardNumeric", bundle: nil)
        let objects = nib.instantiateWithOwner(self, options: nil)
        view = objects[0] as! UIView;
    }
    
    @IBAction func numericSecondKeyboard() {
        let nib = UINib(nibName: "KeyboardNumericSecond", bundle: nil)
        let objects = nib.instantiateWithOwner(self, options: nil)
        view = objects[0] as! UIView;
    }
}
